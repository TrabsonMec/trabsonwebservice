<?php
$sql = "SELECT * FROM lane;";
//$query = mysqli_query($link, $sql);
?>

<h2>Bem-vindo(a) ao Contato</h2>
<p>Página dedicada ao contato com o site!</p> 
<h1>Formulário</h1>
<form action="?pg=processar_formulario" id="formulario" class="form" method="POST">
  <form>
    <div class="form-group">
      <label for="formGroupExampleInput">Nome Completo</label>
      <input required type="text" class="form-control" id="formGroupExampleInput" name="nome" placeholder="Digite seu Nome Completo">
    </div>
    <div class="form-group">
      <td>
        <input required type="radio" id="sexoM" name="sexo" value="M"> Masculino
        <input required type="radio" id="sexoF" name="sexo" value="F"> Feminino
      </td>
    </div>
    <div class="form-group">
      <label for="exampleInputEmail1">Endereço de Email</label>
      <input required type="email" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Digite seu Email">
      <small id="emailHelp" class="form-text text-muted">Não compartilharemos seu Email com ninguém!</small>
    </div>
    <div class="form-group">
      <label for="exampleInputPassword1">Senha</label>
      <input required type="password" name="senha" class="form-control" id="exampleInputPassword1" placeholder="Digite sua Senha">
    </div>
    <div class="form-group">
      <label for="exampleTelefone">Telefone</label>
      <input required type="text" name="telefone" class="form-control" id="exampleTelefone" placeholder="00 999999999">
    </div>

    <div>
      <p>
        Lane Primária:
        <select name="lanePrimaria">
          <?php
          $query = mysqli_query($link, $sql);
          while ($row = mysqli_fetch_assoc($query)) {
            $id = $row["id"];
            $nome = $row["lane"];
            // $categoria = $row["categoria"];
            // $valor = $valor["valor"];
            // $foto = $row["foto"];
            echo '<option value="'.$id.'">'.$nome.'</option>';
          }
          ?>
        </select>
    </div>
    
    <div>

        <p>
          Lane Secundária:
        <select name="laneSecundaria">
          <?php
          $query = mysqli_query($link, $sql);
          while ($row = mysqli_fetch_assoc($query)) {
            $id = $row["id"];
            $nome = $row["lane"];
            // $categoria = $row["categoria"];
            // $valor = $valor["valor"];
            // $foto = $row["foto"];

            echo '<option value="'.$id.'">'.$nome.'</option>';
          }
          ?>
        </select>
      </div><!-- 
      <div class="form-group form-check">
        <input required type="checkbox" class="form-check-input" id="exampleCheck1">
        <label class="form-check-label" for="exampleCheck1">Concordo com os termos de uso.</label>
      </div> -->
      <button type="submit" name="enviar" class="btn btn-primary">Enviar</button>
    </form>
  </form>         
