<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);
session_start();

require_once("configuracoes/conexaoDB.php");

if(isset($_GET["pg"])){
  $pg = $_GET["pg"];
}
else{
  $pg = "inicio";
}

?>

<!doctype html>
<html lang="en">
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="bootstrap-4.1.0/favicon.ico">

    <title>Meu Site</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap-4.1.0/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/carousel.css" rel="stylesheet">
  </head>

    <body id="corpo">

    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item <?= ($pg == 'inicio')?'active':'' ?>">
            <a class="nav-link" href="./">Home</a>
          </li>
          
          <li class="nav-item 
          <?php 

            if($pg == 'sobre'){
              echo 'active';
            }

          ?>
          ">
            <a class="nav-link" href="?pg=sobre">Sobre</a>
          </li>
          <li class="nav-item 
          <?php 

            if($pg == 'listaItens'){
              echo 'active';
            }

          ?>
          ">
            <a class="nav-link" href="?pg=listaItens">Listagem de Itens</a>
          </li>
          <li class="nav-item 
          <?php 

            if($pg == 'formulario'){
              echo 'active';
            }

          ?>
          ">
            <a class="nav-link" href="?pg=formulario">Contato</a>
          </li>
          
          <li class="nav-item 
          <?php 

            if($pg == 'areaRestrita'){
              echo 'active';
            }

          ?>
          ">
            <a class="nav-link" href="?pg=areaRestrita">Área Restrita</a>
          </li>
        </ul>
      </div>
      <li class="nav-item 
          <?php 

            if($pg == 'formularioLogin'){
              echo 'active';
            }

          ?>
          ">
            <a class="nav-link" href="?pg=formularioLogin">Login</a>
      </li>
    <!-- <nav class="navbar navbar-light bg-dark">
      <form class="form-inline">
      <input class="form-control mr-sm-2" type="search" placeholder="Digite o que procura" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
      </form>
    </nav> -->
    </nav>

    <main role="main">

      <!-- Main jumbotron for a primary marketing message or call to action -->
      <div class="jumbotron">
        <div class="container">
          <h1 class="display-3">Meu site!</h1>
          <p>Este é um site de exemplo.</p>
          <?php
            if (isset($_SESSION['usuario'])) {
              echo "<p>Olá ". $_SESSION['usuario']['nome'] ."<a href='?pg=logout'>      Sair</a>"."</p>" ;
              //echo "<a href='?pg=logout'>Sair</a>";
            }
          ?>

          <!--<form method="POST">
            <input type="color" name="corFundo" id="corFundo">
            <input type="submit" name="alterarCor" id="alterarCor" value="Alterar cor!">
          </form>-->

        </div>
      </div>

      <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <?php
        if($pg=='formulario'){
           echo '<li class="breadcrumb-item"><a href="#">Formulário</a></li>';
        }
        if($pg == 'sobre'){
      	   echo '<li class="breadcrumb-item"><a href="#">Sobre</a></li>';
        }
        if($pg == 'listaItens'){
      	   echo '<li class="breadcrumb-item"><a href="#">Lista de Itens</a></li>';
        }
        if($pg == 'formularioLogin'){
           echo '<li class="breadcrumb-item"><a href="#">Login</a></li>';
        }
        if($pg == 'areaRestrita'){
           echo '<li class="breadcrumb-item"><a href="#">Área Restrita</a></li>';
        }
      ?>
    </ol>
      </nav>
      <div class="container">
        <!-- Example row of columns -->
        <div class="row">
          <div class="col">
            <?php include("paginas/".$pg.".php"); ?>
          </div>
        </div>

        <hr>

      </div> <!-- /container -->

    </main>

    <footer class="container">
      <p>&copy; Meu projeto 2018</p>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="bootstrap-4.1.0/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="bootstrap-4.1.0/assets/js/vendor/popper.min.js"></script>
    <script src="bootstrap-4.1.0/dist/js/bootstrap.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="bootstrap-4.1.0/assets/js/vendor/holder.min.js"></script>
  </body>
</html>
